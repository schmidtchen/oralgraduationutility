package ogup;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ogup.data.UtilityManager;

import static ogup.data.UtilityManager.FONT_SIZE_CHANGE_WIDTH;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        UtilityManager utilityManager = UtilityManager.getUtilityManager();
        utilityManager.createTables();

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        Scene scene = new Scene(root, 600, 300);

        stage.setTitle("OGUP");
        stage.setScene(scene);
        stage.setMinWidth(600);
        stage.setMinHeight(300);

        stage.setOnCloseRequest(event -> {
            if (utilityManager.isCloseAlert()) {
                utilityManager.confirmDiscard(result -> {
                    if (!result) {
                        event.consume();
                    }
                });
            }
        });

        stage.getIcons().add(new Image(getClass().getResourceAsStream("/icon/icon.png")));
        stage.show();

        stage.widthProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > FONT_SIZE_CHANGE_WIDTH) {
                stage.getScene().getRoot().setStyle("-fx-font-size: 16px");
            } else {
                stage.getScene().getRoot().setStyle("-fx-font-size: 13px");
            }
        });

        stage.sceneProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.getWidth() > FONT_SIZE_CHANGE_WIDTH) {
                newValue.getRoot().setStyle("-fx-font-size: 16px");
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }


}
