package ogup.models;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Evaluation {

    private String considerations;
    private String comments;
    private int points;

    public String getConsiderations() {
        return considerations;
    }

    public void setConsiderations(String considerations) {
        this.considerations = considerations;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
