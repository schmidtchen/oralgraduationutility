package ogup.models;

import java.time.LocalDate;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Student {

    private String name;
    private LocalDate birthday;
    private String birthplace;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }
}
