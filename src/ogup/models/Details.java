package ogup.models;

import java.time.LocalDate;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Details {

    private String subject;
    private LocalDate graduationDate;
    private String startTime;
    private String endTime;
    private String chairman;
    private String examiningTeacher;
    private String secretary;
    private String furtherMembers;
    private boolean entitledToVote;
    private String qualifiedObservers;
    private String furtherObservers;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(LocalDate graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChairman() {
        return chairman;
    }

    public void setChairman(String chairman) {
        this.chairman = chairman;
    }

    public String getExaminingTeacher() {
        return examiningTeacher;
    }

    public void setExaminingTeacher(String examiningTeacher) {
        this.examiningTeacher = examiningTeacher;
    }

    public String getSecretary() {
        return secretary;
    }

    public void setSecretary(String secretary) {
        this.secretary = secretary;
    }

    public String getFurtherMembers() {
        return furtherMembers;
    }

    public void setFurtherMembers(String furtherMembers) {
        this.furtherMembers = furtherMembers;
    }

    public String getQualifiedObservers() {
        return qualifiedObservers;
    }

    public void setQualifiedObservers(String qualifiedObservers) {
        this.qualifiedObservers = qualifiedObservers;
    }

    public String getFurtherObservers() {
        return furtherObservers;
    }

    public void setFurtherObservers(String furtherObservers) {
        this.furtherObservers = furtherObservers;
    }

    public boolean isEntitledToVote() {
        return entitledToVote;
    }

    public void setEntitledToVote(boolean entitledToVote) {
        this.entitledToVote = entitledToVote;
    }
}
