package ogup.models;

import javafx.scene.control.Alert;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import ogup.Main;
import ogup.data.UtilityManager;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Protocol {

    private Student student;
    private Evaluation evaluation;
    private Details details;

    private Map<String, Boolean> attachments;

    private int protocolID;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public Map<String, Boolean> getAttachments() {
        return attachments;
    }

    public void setAttachments(Map<String, Boolean> attachments) {
        this.attachments = attachments;
    }

    public Protocol(){
        protocolID = -1;
    }

    public Protocol(int id) {
        protocolID = id;

        ResultSet resultSet = UtilityManager.getUtilityManager().getResult("SELECT * FROM graduations NATURAL JOIN subjects WHERE protocol_id = " + id);
        try {
            if (resultSet != null && resultSet.next()) {
                System.out.println("Protokoll von " + resultSet.getString("student_name") + " wird geladen...");
                student = new Student();
                student.setName(resultSet.getString("student_name"));
                student.setBirthday(resultSet.getDate("birthday") != null ? resultSet.getDate("birthday").toLocalDate() : LocalDate.now());
                student.setBirthplace(resultSet.getString("birthplace"));

                details = new Details();
                details.setSubject(resultSet.getString("subject"));
                details.setGraduationDate(resultSet.getDate("graduation_date") != null ? resultSet.getDate("graduation_date").toLocalDate() : LocalDate.now());
                details.setStartTime(resultSet.getString("time_start"));
                details.setEndTime(resultSet.getString("time_end"));
                details.setChairman(resultSet.getString("chairman"));
                details.setExaminingTeacher(resultSet.getString("examining_teacher"));
                details.setSecretary(resultSet.getString("secretary"));
                details.setFurtherMembers(resultSet.getString("further_members"));
                details.setEntitledToVote(resultSet.getBoolean("entitled_to_vote"));
                details.setQualifiedObservers(resultSet.getString("qualified_observers"));
                details.setFurtherObservers(resultSet.getString("further_observers"));

                evaluation = new Evaluation();
                evaluation.setConsiderations(resultSet.getString("considerations"));
                evaluation.setComments(resultSet.getString("comments"));
                evaluation.setPoints(resultSet.getInt("points"));

                resultSet.close();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setHeaderText("Fehler beim Laden des Dokuments!");
                alert.setContentText("Der Datenbankeintrag des Schülers ist beschädigt!");

                alert.showAndWait();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void storeInDatabase() {
        if (protocolID > -1) {
            UtilityManager.getUtilityManager().executeStatement("DELETE FROM graduations WHERE protocol_id = " + protocolID);
        }

        String sqlQuery = "INSERT INTO graduations (" +
                "student_name, " +
                "birthday, " +
                "birthplace, " +
                "subject_id, " +
                "graduation_date, " +
                "time_start, " +
                "time_end, " +
                "chairman, " +
                "examining_teacher, " +
                "secretary, " +
                "further_members, " +
                "entitled_to_vote, " +
                "qualified_observers, " +
                "further_observers, " +
                "considerations, " +
                "comments, " +
                "points, protocol_id) VALUES (?, ?, ?, (SELECT subject_id FROM subjects WHERE subject = '" + details.getSubject() + "'), ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ? , ?, ?, " + (protocolID > -1 ? protocolID : "DEFAULT") + ")";

        PreparedStatement preparedStatement = UtilityManager.getUtilityManager().prepareStatement(sqlQuery);
        if (preparedStatement != null) {
            try {
                preparedStatement.setString(1, student.getName());
                if (student.getBirthday() != null) {
                    preparedStatement.setDate(2, Date.valueOf(student.getBirthday()));
                } else {
                    preparedStatement.setNull(2, Types.DATE);
                }
                preparedStatement.setString(3, student.getBirthplace());

                if (details.getGraduationDate() != null) {
                    preparedStatement.setDate(4, Date.valueOf(details.getGraduationDate()));
                } else {
                    preparedStatement.setNull(4, Types.DATE);
                }
                preparedStatement.setString(5, details.getStartTime());
                preparedStatement.setString(6, details.getEndTime());
                preparedStatement.setString(7, details.getChairman());
                preparedStatement.setString(8, details.getExaminingTeacher());
                preparedStatement.setString(9, details.getSecretary());
                preparedStatement.setString(10, details.getFurtherMembers());
                preparedStatement.setBoolean(11, details.isEntitledToVote());
                preparedStatement.setString(12, details.getQualifiedObservers());
                preparedStatement.setString(13, details.getFurtherObservers());
                preparedStatement.setString(14, evaluation.getConsiderations());
                preparedStatement.setString(15, evaluation.getComments());
                preparedStatement.setInt(16, evaluation.getPoints());

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createPDF() {
        try {
            Preferences preferences = Preferences.userNodeForPackage(Main.class);

            File template = new File(preferences.get("directory", System.getProperty("user.dir")) + "/OGUP/template.pdf");
            if (!template.exists()) {
                try(OutputStream outputStream = new FileOutputStream(template)){
                    IOUtils.copy(getClass().getResourceAsStream("/template.pdf"), outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            PDDocument document = PDDocument.load(template);

            PDDocumentCatalog documentCatalog = document.getDocumentCatalog();
            PDAcroForm acroForm = documentCatalog.getAcroForm();

            if (acroForm != null) {
                String[] nameSplit = getStudent().getName().split(" ");
                StringBuilder name = new StringBuilder();
                name.append(nameSplit[nameSplit.length - 1]).append(", ");
                for (int i = 0; i < nameSplit.length-1; i++) {
                    name.append(nameSplit[i]).append(" ");
                }

                fillField(acroForm, "year", "Abitur " + getDetails().getGraduationDate().getYear());
                fillField(acroForm, "name", name.toString().trim());
                fillField(acroForm, "birthday", getStudent().getBirthday() != null ? getStudent().getBirthday().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) : "");
                fillField(acroForm, "birthplace", getStudent().getBirthplace());

                fillField(acroForm, "subject", getDetails().getSubject());
                fillField(acroForm, "date", getDetails().getGraduationDate() != null ? getDetails().getGraduationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) : "");
                fillField(acroForm, "start_time", getDetails().getStartTime());
                fillField(acroForm, "end_time", getDetails().getEndTime());
                fillField(acroForm, "chairman", getDetails().getChairman());
                fillField(acroForm, "examining_teacher", getDetails().getExaminingTeacher());
                fillField(acroForm, "secretary", getDetails().getSecretary());
                fillField(acroForm, "further_members", getDetails().getFurtherMembers());
                fillField(acroForm, "entitled_to_vote", getDetails().isEntitledToVote() ? "Yes" : "Off");
                fillField(acroForm, "not_entitled_to_vote", getDetails().getFurtherMembers().isEmpty() || getDetails().isEntitledToVote() ? "Off" : "Yes");
                fillField(acroForm, "qualified_observers", getDetails().getQualifiedObservers());
                fillField(acroForm, "further_observers", getDetails().getFurtherObservers());

                fillField(acroForm, "considerations", getEvaluation().getConsiderations());
                fillField(acroForm, "comments", getEvaluation().getComments());
                fillField(acroForm, "points", String.valueOf(getEvaluation().getPoints()));
                fillField(acroForm, "grade", UtilityManager.getUtilityManager().calculateGrade(getEvaluation().getPoints()));

                for (String attachment : attachments.keySet()) {
                    fillField(acroForm, attachment, attachments.get(attachment) ? "Yes" : "Off");
                }
            }

            File directory = new File(preferences.get("directory", System.getProperty("user.dir")) + "/OGUP/protocols");
            directory.mkdirs();
            File studentDocument = new File(directory.getAbsolutePath() + "/" + getStudent().getName().replaceAll("\\s+", "") + "_" + (getStudent().getBirthday() != null ? getStudent().getBirthday().toString() : "0000-00-00") + ".pdf");
            document.save(studentDocument);
            document.close();

            Desktop.getDesktop().open(studentDocument);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillField(PDAcroForm acroForm, String key, String value) {
        try {
            PDField field = acroForm.getField(key);
            if (field != null) {
                field.setValue(value != null ? value : "");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
