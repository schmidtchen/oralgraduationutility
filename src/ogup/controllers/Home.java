package ogup.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import ogup.Main;
import ogup.data.UtilityManager;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

public class Home implements Initializable {

    @FXML VBox top_menu;

    @FXML ListView<String> protocol_list;

    private List<Integer> currentProtocolList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadListContent();

        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("Protokoll löschen");
        contextMenu.getItems().add(menuItem);

        menuItem.setOnAction(event -> askForConfirmation());
        protocol_list.setContextMenu(contextMenu);

        protocol_list.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && protocol_list.getSelectionModel().getSelectedItem() != null) {
                try {
                    openProtocol(currentProtocolList.get(protocol_list.getSelectionModel().getSelectedIndex()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        protocol_list.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.BACK_SPACE && protocol_list.getSelectionModel().getSelectedItem() != null) {
                askForConfirmation();
            }
        });

        MenuBar menuBar = new MenuBar();
        Menu settings = new Menu("Einstellungen");
        MenuItem setDirectory = new MenuItem("Programmordner festlegen...");
        setDirectory.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Ordner auswählen");
            File file = directoryChooser.showDialog(top_menu.getScene().getWindow());

            if (file != null) {
                Preferences preferences = Preferences.userNodeForPackage(Main.class);
                String oldPath = preferences.get("directory", System.getProperty("user.dir")) + "/OGUP";
                File oldFiles = new File(oldPath);

                if (!oldFiles.getAbsolutePath().equals(file.getAbsolutePath() + "/OGUP")) {
                    preferences.put("directory", file.getAbsolutePath());
                    try {
                        FileUtils.copyDirectory(oldFiles, new File(file.getAbsolutePath() + "/OGUP"));
                        File protocols = new File(oldFiles.getAbsolutePath() + "/protocols");
                        if (protocols.exists()) {
                            FileUtils.copyDirectory(protocols, new File(file.getAbsolutePath() + "/OGUP/protocols"));
                        }
                        FileUtils.deleteDirectory(oldFiles);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Neuer Programmordner: " + file.getAbsolutePath());
                }
            }
        });

        MenuItem addSubject = new MenuItem("Prüfungsfächer bearbeiten...");
        addSubject.setOnAction(event -> {
            if (!((Stage) top_menu.getScene().getWindow()).getTitle().equals("OGUP")) {
                UtilityManager.getUtilityManager().confirmDiscard(result -> {
                    if (result) {
                        openSubjects(((Stage) top_menu.getScene().getWindow()));
                    }
                });
            } else {
                openSubjects(((Stage) top_menu.getScene().getWindow()));
            }
        });

        settings.getItems().addAll(setDirectory, addSubject);
        menuBar.getMenus().addAll(settings);
        top_menu.getChildren().add(0, menuBar);

        UtilityManager.getUtilityManager().setCloseAlert(false);
    }

    private void askForConfirmation() {
        if (protocol_list.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Löschen bestätigen");
            alert.setHeaderText("Protokoll wirklich löschen?");
            alert.setContentText("Soll das Protokoll von " + protocol_list.getSelectionModel().getSelectedItem() + " wirklich unwiderruflich gelöscht werden?");
            ButtonType confirm = new ButtonType("Ja", ButtonBar.ButtonData.YES);
            ButtonType decline = new ButtonType("Nein", ButtonBar.ButtonData.NO);

            alert.getButtonTypes().setAll(confirm, decline);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == confirm) {
                deleteProtocol(currentProtocolList.get(protocol_list.getSelectionModel().getSelectedIndex()));
                protocol_list.getItems().remove(protocol_list.getSelectionModel().getSelectedIndex());
            }
        }
    }

    private void loadListContent() {
        currentProtocolList = new ArrayList<>();
        protocol_list.getItems().clear();

        ResultSet resultSet = UtilityManager.getUtilityManager().getResult("SELECT protocol_id, student_name FROM graduations");
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    currentProtocolList.add(resultSet.getInt("protocol_id"));
                    protocol_list.getItems().add(resultSet.getString("student_name"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void openProtocol(int id) throws IOException {
        if (!UtilityManager.getUtilityManager().getSubjects().isEmpty()) {
            Stage currentStage = (Stage) protocol_list.getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/frame.fxml"));
            VBox frame = fxmlLoader.load();
            Frame frameController = fxmlLoader.getController();
            if (frameController.loadDocument(id)) {
                currentStage.setTitle("Prüfungsdokument bearbeiten");
                currentStage.setScene(new Scene(frame, currentStage.getWidth(), currentStage.getHeight()));
            }
        } else {
            showWarning();
        }
    }

    private void deleteProtocol(int id) {
        currentProtocolList.remove((Integer) id);
        UtilityManager.getUtilityManager().executeStatement("DELETE FROM graduations WHERE protocol_id = " + id);
        System.out.println("Protokoll von " + protocol_list.getSelectionModel().getSelectedItem() + " wurde gelöscht!");
    }

    public void createNewOralGraduation(ActionEvent actionEvent) throws Exception {
        if (!UtilityManager.getUtilityManager().getSubjects().isEmpty()) {
            Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/frame.fxml"));
            VBox frame = fxmlLoader.load();

            currentStage.setTitle("Neues Prüfungsdokument");
            currentStage.setScene(new Scene(frame, currentStage.getWidth(), currentStage.getHeight()));
        } else {
            showWarning();
        }
    }

    public void editConsiderations(ActionEvent actionEvent) throws IOException {
        Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        BorderPane editConsiderations = FXMLLoader.load(getClass().getResource("/fxml/editconsiderations.fxml"));

        currentStage.setTitle("Erwägungen bearbeiten");
        currentStage.setScene(new Scene(editConsiderations, currentStage.getWidth(), currentStage.getHeight()));
    }

    private void openSubjects(Stage currentStage) {
        try {
            currentStage.setTitle("Prüfungsfächer bearbeiten");
            BorderPane editSubjects = FXMLLoader.load(getClass().getResource("/fxml/editsubjects.fxml"));

            currentStage.setScene(new Scene(editSubjects, currentStage.getWidth(), currentStage.getHeight()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Prüfungsfächer");
        alert.setHeaderText("Keine Prüfungsfächer vorhanden!");
        alert.setContentText("Sie sollten vor dem ersten Öffnen einige Prüfungsfächer hinzufügen! Klicken Sie dafür in den Einstellungen auf 'Prüfungsfächer bearbeiten...'.");

        alert.showAndWait();
    }
}
