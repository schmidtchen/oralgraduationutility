package ogup.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class FoldCommittee {

    @FXML
    TextField chairman;

    @FXML
    TextField examiningTeacher;

    @FXML
    TextField secretary;

    @FXML
    TextArea furtherMembers;

    @FXML
    CheckBox entitledToVote;

    public String getChairman() {
        return chairman.getText();
    }

    public String getExaminingTeacher() {
        return examiningTeacher.getText();
    }

    public String getSecretary() {
        return secretary.getText();
    }

    public String getFurtherMembers() {
        return furtherMembers.getText();
    }

    public boolean isEntitledToVote() {
        return entitledToVote.isSelected();
    }

    public void setChairman(String chairman) {
        this.chairman.setText(chairman);
    }

    public void setExaminingTeacher(String examiningTeacher) {
        this.examiningTeacher.setText(examiningTeacher);
    }

    public void setSecretary(String secretary) {
        this.secretary.setText(secretary);
    }

    public void setFurtherMembers(String furtherMembers) {
        this.furtherMembers.setText(furtherMembers);
    }

    public void setEntitledToVote(boolean entitledToVote) {
        this.entitledToVote.setSelected(entitledToVote);
    }
}
