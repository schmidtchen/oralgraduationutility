package ogup.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ogup.data.UtilityManager;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Matti on 2019-04-29.
 * » http://schmidtchen.me «
 */
public class EditSubjects implements Initializable {

    @FXML
    VBox subjectBox;

    private List<String> dbSubjects;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilityManager.getUtilityManager().setCloseAlert(true);

        loadSubjectList();
    }

    private void loadSubjectList() {
        subjectBox.getChildren().removeIf(node -> node instanceof HBox);

        dbSubjects = UtilityManager.getUtilityManager().getSubjects();

        for (String subject : dbSubjects) {
            subjectBox.getChildren().add(subjectBox.getChildren().size()-1, UtilityManager.getUtilityManager().createInputItem(subject));
        }
    }

    public void discardChanges(ActionEvent event) {
        dbSubjects.clear();
        UtilityManager.getUtilityManager().discardChanges(event);
    }

    public void saveChanges(ActionEvent event) throws IOException {
        List<String> updatedSubjects = new ArrayList<>();

        for (Node node : subjectBox.getChildren()) {
            if (node instanceof HBox) {
                HBox subjectItem = (HBox) node;
                for (Node child : subjectItem.getChildren()) {
                    if (child instanceof TextField) {
                        TextField subject = (TextField) child;
                        updatedSubjects.add(subject.getText());
                        if (!subject.getText().isEmpty() && !dbSubjects.contains(subject.getText())) {
                            UtilityManager.getUtilityManager().executeStatement("INSERT INTO subjects (subject) VALUES ('" + subject.getText()+ "')");
                        }
                    }
                }
            }
        }

        boolean error = false;
        for (String subject : dbSubjects) {
            if (!updatedSubjects.contains(subject)) {
                ResultSet resultSet = UtilityManager.getUtilityManager().getResult("SELECT COUNT(*) AS documents FROM graduations NATURAL JOIN subjects WHERE subject = '" + subject + "'");
                try {
                    if (resultSet != null && resultSet.next()) {
                        int count = resultSet.getInt("documents");
                        resultSet.close();
                        if (count == 0) {
                            UtilityManager.getUtilityManager().executeStatement("DELETE FROM subjects WHERE subject = '" + subject + "'");
                        } else {
                            error = true;

                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Fehler beim Speichern");
                            alert.setHeaderText(subject + " kann nicht gelöscht werden!");
                            alert.setContentText("Dieses Prüfungsfach wurde bereits in einem Prüfungsdokument verwendet und kann nicht mehr entfernt werden!");

                            alert.showAndWait();
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!error) {
            Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            BorderPane home = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));

            currentStage.setTitle("OGUP");
            currentStage.setScene(new Scene(home, currentStage.getWidth(), currentStage.getHeight()));
        } else {
            loadSubjectList();
        }
    }

    public void addSubject(ActionEvent event) {
        subjectBox.getChildren().add(subjectBox.getChildren().size()-1, UtilityManager.getUtilityManager().createInputItem(null));
    }
}
