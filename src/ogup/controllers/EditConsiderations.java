package ogup.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ogup.data.UtilityManager;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class EditConsiderations implements Initializable  {

    @FXML
    ChoiceBox<String> chooseGrade;

    @FXML
    VBox considerationBox;

    @FXML
    Button addConsiderationButton;

    private HashMap<Integer, List<String>> oldConsiderations, updatedConsiderations;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chooseGrade.getItems().addAll(UtilityManager.getUtilityManager().getGradeNames().values());
        chooseGrade.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> handleChoiceBox(oldValue, newValue));

        addConsiderationButton.setVisible(false);

        oldConsiderations = new HashMap<>();
        updatedConsiderations = new HashMap<>();

        UtilityManager.getUtilityManager().setCloseAlert(true);
    }

    private void handleChoiceBox(String oldValue, String newValue) {
        if (oldValue != null) {
            storeCurrentConsiderations(oldValue);
        }

        int grade = getGradeFromGradeName(newValue);
        considerationBox.getChildren().removeIf(node -> node instanceof HBox);

        List<String> considerations;
        if (!updatedConsiderations.containsKey(grade)) {
            considerations = UtilityManager.getUtilityManager().getConsiderations(grade);
            oldConsiderations.put(grade, considerations);
        } else {
            considerations = updatedConsiderations.get(grade);
        }

        for (String consideration : considerations) {
            considerationBox.getChildren().add(considerationBox.getChildren().size()-1, UtilityManager.getUtilityManager().createInputItem(consideration));
        }
        addConsiderationButton.setVisible(true);
    }

    public void addConsideration(ActionEvent event) {
        considerationBox.getChildren().add(considerationBox.getChildren().size()-1, UtilityManager.getUtilityManager().createInputItem(null));
    }

    public void discardChanges(ActionEvent actionEvent) {
        updatedConsiderations.clear();

        UtilityManager.getUtilityManager().discardChanges(actionEvent);
    }

    public void saveChanges(ActionEvent actionEvent) throws IOException {
        if (chooseGrade.getValue() != null) {
            storeCurrentConsiderations(chooseGrade.getValue());
        }

        for (int grade : updatedConsiderations.keySet()) {
            for (String consideration : updatedConsiderations.get(grade)) {
                if (!oldConsiderations.get(grade).contains(consideration)) {
                    UtilityManager.getUtilityManager().executeStatement("INSERT INTO considerations (grade, consideration) VALUES (" + grade + ", '" + consideration + "')");
                }
            }
        }

        for (int grade : oldConsiderations.keySet()) {
            for (String consideration : oldConsiderations.get(grade)) {
                if (!updatedConsiderations.get(grade).contains(consideration)) {
                    UtilityManager.getUtilityManager().executeStatement("DELETE FROM considerations WHERE consideration = '" + consideration + "'");
                }
            }
        }

        Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        BorderPane home = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));

        currentStage.setTitle("OGUP");
        currentStage.setScene(new Scene(home, currentStage.getWidth(), currentStage.getHeight()));
    }

    private int getGradeFromGradeName(String gradeName) {
        return UtilityManager.getUtilityManager().getGradeNames().entrySet().stream().filter(entry -> entry.getValue().equals(gradeName)).findFirst().get().getKey();
    }

    private void storeCurrentConsiderations(String gradeName) {
        List<String> currentConsiderations = new ArrayList<>();

        for (Node node : considerationBox.getChildren()) {
            if (node instanceof HBox) {
                HBox considerationItem = (HBox) node;
                for (Node child : considerationItem.getChildren()) {
                    if (child instanceof TextField) {
                        TextField consideration = (TextField) child;
                        if (!consideration.getText().isEmpty()) {
                            currentConsiderations.add(consideration.getText());
                        }
                    }
                }
            }
        }

        updatedConsiderations.put(getGradeFromGradeName(gradeName), currentConsiderations);
    }
}
