package ogup.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Observers {

    @FXML
    TextArea qualifiedObservers;

    @FXML
    TextArea furtherObservers;

    public String getQualifiedObservers() {
        return qualifiedObservers.getText();
    }

    public String getFurtherObservers() {
        return furtherObservers.getText();
    }

    public void setQualifiedObservers(String qualifiedObservers) {
        this.qualifiedObservers.setText(qualifiedObservers);
    }

    public void setFurtherObservers(String furtherObservers) {
        this.furtherObservers.setText(furtherObservers);
    }
}
