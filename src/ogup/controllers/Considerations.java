package ogup.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import ogup.data.UtilityManager;

import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class Considerations implements Initializable {

    @FXML
    TextArea considerations;

    @FXML
    MenuButton addConsideration;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<Menu> grades = new ArrayList<>();
        for (String gradeName : UtilityManager.getUtilityManager().getGradeNames().values()) {
            grades.add(new Menu(gradeName));
        }

        addConsideration.getItems().addAll(grades);

        EventHandler<ActionEvent> chooseConsideration = event -> {
            MenuItem clickedConsideration = (MenuItem) event.getSource();

            considerations.appendText((considerations.getText().isEmpty() ? "" : " ") + clickedConsideration.getText());
        };

        try {
            ResultSet resultSet = UtilityManager.getUtilityManager().getResult("SELECT grade, consideration FROM considerations");

            while (resultSet.next()) {
                MenuItem consideration = new MenuItem(resultSet.getString("consideration"));
                grades.get(resultSet.getInt("grade") - 1).getItems().add(consideration);
                consideration.setOnAction(chooseConsideration);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }


    public String getConsiderations() {
        return considerations.getText();
    }

    public void setConsiderations(String considerations) {
        this.considerations.setText(considerations);
    }
}
