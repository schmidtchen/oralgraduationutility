package ogup.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ogup.data.UtilityManager;
import ogup.models.Details;
import ogup.models.Evaluation;
import ogup.models.Protocol;
import ogup.models.Student;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;

/**
 * Created by Matti on 2019-02-01.
 * » http://schmidtchen.me «
 */
public class Frame implements Initializable {

    @FXML StudentDetails studentDetailsController;
    @FXML TestingDetails testingDetailsController;
    @FXML FoldCommittee foldCommitteeController;
    @FXML Observers observersController;
    @FXML Considerations considerationsController;
    @FXML CommentsAndGrade commentsAndGradeController;
    @FXML Attachments attachmentsController;
    @FXML TabPane document_tabs;

    private Protocol currentProtocol;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilityManager.getUtilityManager().setCloseAlert(true);
    }

    public boolean loadDocument(int id) {
        currentProtocol = new Protocol(id);

        if (currentProtocol.getStudent() == null || currentProtocol.getDetails() == null || currentProtocol.getEvaluation() == null) {
            return false;
        }

        studentDetailsController.setStudentName(currentProtocol.getStudent().getName());
        studentDetailsController.setBirthday(currentProtocol.getStudent().getBirthday());
        studentDetailsController.setBirthplace(currentProtocol.getStudent().getBirthplace());

        testingDetailsController.setSubject(currentProtocol.getDetails().getSubject());
        testingDetailsController.setDate(currentProtocol.getDetails().getGraduationDate());
        testingDetailsController.setStart(currentProtocol.getDetails().getStartTime());
        testingDetailsController.setEnd(currentProtocol.getDetails().getEndTime());

        foldCommitteeController.setChairman(currentProtocol.getDetails().getChairman());
        foldCommitteeController.setExaminingTeacher(currentProtocol.getDetails().getExaminingTeacher());
        foldCommitteeController.setSecretary(currentProtocol.getDetails().getSecretary());
        foldCommitteeController.setFurtherMembers(currentProtocol.getDetails().getFurtherMembers());
        foldCommitteeController.setEntitledToVote(currentProtocol.getDetails().isEntitledToVote());

        observersController.setQualifiedObservers(currentProtocol.getDetails().getQualifiedObservers());
        observersController.setFurtherObservers(currentProtocol.getDetails().getFurtherObservers());

        considerationsController.setConsiderations(currentProtocol.getEvaluation().getConsiderations());

        commentsAndGradeController.setComments(currentProtocol.getEvaluation().getComments());
        commentsAndGradeController.setPoints(currentProtocol.getEvaluation().getPoints());

        System.out.println("Protokoll von " + currentProtocol.getStudent().getName() + " geladen!");

        return true;
    }

    public void discardDraft(ActionEvent actionEvent) {
        UtilityManager.getUtilityManager().confirmDiscard(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean result) {
                if (result) {
                    Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                    BorderPane home;
                    try {
                        home = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
                        currentStage.setTitle("OGUP");
                        currentStage.setScene(new Scene(home, currentStage.getWidth(), currentStage.getHeight()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void save(ActionEvent actionEvent) throws IOException {
        if (currentProtocol == null) {
            currentProtocol = new Protocol();
        }

        Student student = new Student();
        student.setName(studentDetailsController.getStudentName());
        student.setBirthday(studentDetailsController.getBirthday());
        student.setBirthplace(studentDetailsController.getBirthplace());

        if (student.getName().isEmpty() || student.getBirthday() == null || student.getBirthplace().isEmpty()) {
            displayError(0);
            return;
        }

        currentProtocol.setStudent(student);

        Details details = new Details();
        details.setSubject(testingDetailsController.getSubject());
        details.setGraduationDate(testingDetailsController.getDate());
        details.setStartTime(testingDetailsController.getStart());
        details.setEndTime(testingDetailsController.getEnd());

        if (details.getSubject() == null || details.getGraduationDate() == null || details.getStartTime().isEmpty() || details.getEndTime().isEmpty()) {
            displayError(1);
            return;
        }

        if (!UtilityManager.getUtilityManager().validateTime(details.getStartTime()) || !UtilityManager.getUtilityManager().validateTime(details.getEndTime())) {
            document_tabs.getSelectionModel().select(1);
            UtilityManager.getUtilityManager().displayFormatError();
            return;
        }

        details.setChairman(foldCommitteeController.getChairman());
        details.setExaminingTeacher(foldCommitteeController.getExaminingTeacher());
        details.setSecretary(foldCommitteeController.getSecretary());
        details.setFurtherMembers(foldCommitteeController.getFurtherMembers());
        details.setEntitledToVote(foldCommitteeController.isEntitledToVote());

        if (details.getChairman().isEmpty() || details.getExaminingTeacher().isEmpty() || details.getSecretary().isEmpty()) {
            displayError(2);
            return;
        }

        details.setQualifiedObservers(observersController.getQualifiedObservers());
        details.setFurtherObservers(observersController.getFurtherObservers());

        currentProtocol.setDetails(details);

        Evaluation evaluation = new Evaluation();
        evaluation.setConsiderations(considerationsController.getConsiderations());
        evaluation.setComments(commentsAndGradeController.getComments());
        evaluation.setPoints(commentsAndGradeController.getPoints());

        if (evaluation.getConsiderations().isEmpty() || evaluation.getPoints() < 0 || evaluation.getPoints() > 15) {
            displayError(evaluation.getConsiderations().isEmpty() ? 4 : 5);
            return;
        }

        currentProtocol.setEvaluation(evaluation);

        Map<String, Boolean> attachments = new HashMap<>();
        attachments.put("attachment_protocol", true);
        attachments.put("attachment_tasks", true);
        attachments.put("attachment_material", attachmentsController.isAttachmentMaterial());
        attachments.put("attachment_notes", attachmentsController.isAttachmentNotes());
        attachments.put("attachment_products", attachmentsController.isAttachmentProducts());
        attachments.put("attachment_docs", attachmentsController.isAttachmentDocs());

        currentProtocol.setAttachments(attachments);

        currentProtocol.storeInDatabase();
        currentProtocol.createPDF();

        System.out.println("Protokoll für " + currentProtocol.getStudent().getName() + " gespeichert!");

        Stage currentStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        BorderPane home = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));

        currentStage.setTitle("OGUP");
        currentStage.setScene(new Scene(home, currentStage.getWidth(), currentStage.getHeight()));
    }

    private void displayError(int tabID) {
        Tab tab = document_tabs.getTabs().get(tabID);
        document_tabs.getSelectionModel().select(tab);

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Fehler beim Speichern!");
        alert.setHeaderText("Felder im Reiter '" + tab.getText() + "' unausgefüllt!");
        alert.setContentText("Bitte füllen Sie alle erforderlichen Felder in diesem Reiter aus!");

        alert.showAndWait();
    }
}
