package ogup.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import ogup.data.UtilityManager;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class StudentDetails implements Initializable {

    @FXML
    TextField name;

    @FXML
    DatePicker birthday;

    @FXML
    TextField birthplace;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        birthday.setConverter(UtilityManager.getUtilityManager().getDateConverter());
    }

    public String getStudentName() {
        return name.getText();
    }

    public LocalDate getBirthday() {
        return birthday.getValue();
    }

    public String getBirthplace() {
        return birthplace.getText();
    }

    public void setStudentName(String name) {
        this.name.setText(name);
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.setValue(birthday);
    }

    public void setBirthplace(String birthplace) {
        this.birthplace.setText(birthplace);
    }
}
