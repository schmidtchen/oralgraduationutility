package ogup.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ogup.data.UtilityManager;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class CommentsAndGrade implements Initializable {

    @FXML
    TextArea comments;

    @FXML
    TextField points;

    @FXML
    Label grade;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        points.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                int points = Integer.parseInt(newValue);
                grade.setText("Note: " + UtilityManager.getUtilityManager().calculateGrade(points));
            } catch (NumberFormatException exception) {
                grade.setText("Note: ");
            }
        });
    }

    public String getComments() {
        return comments.getText();
    }

    public int getPoints() {
        try {
            return Integer.parseInt(points.getText());
        } catch (NumberFormatException exception) {
            return -1;
        }
    }

    public void setComments(String comments) {
        this.comments.setText(comments);
    }

    public void setPoints(int points) {
        this.points.setText(String.valueOf(points));
    }
}
