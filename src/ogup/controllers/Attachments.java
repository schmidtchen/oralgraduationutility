package ogup.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

/**
 * Created by Matti on 2019-06-03.
 * » http://schmidtchen.me «
 */
public class Attachments {

    @FXML
    CheckBox attachment_material;

    @FXML
    CheckBox attachment_notes;

    @FXML
    CheckBox attachment_products;

    @FXML
    CheckBox attachment_docs;

    public boolean isAttachmentMaterial() {
        return attachment_material.isSelected();
    }

    public void setAttachmentMaterial(boolean attachment_material) {
        this.attachment_material.setSelected(attachment_material);
    }

    public boolean isAttachmentNotes() {
        return attachment_notes.isSelected();
    }

    public void setAttachmentNotes(boolean attachment_notes) {
        this.attachment_notes.setSelected(attachment_notes);
    }

    public boolean isAttachmentProducts() {
        return attachment_products.isSelected();
    }

    public void setAttachmentProducts(boolean attachment_products) {
        this.attachment_products.setSelected(attachment_products);
    }

    public boolean isAttachmentDocs() {
        return attachment_docs.isSelected();
    }

    public void setAttachmentDocs(boolean attachment_docs) {
        this.attachment_docs.setSelected(attachment_docs);
    }
}
