package ogup.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import ogup.data.UtilityManager;

import java.net.URL;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by Matti on 2019-01-23.
 * » http://schmidtchen.me «
 */
public class TestingDetails implements Initializable {

    @FXML
    ChoiceBox<String> subjectBox;

    @FXML
    DatePicker date;

    @FXML
    TextField start;

    @FXML
    TextField end;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        subjectBox.getItems().addAll(UtilityManager.getUtilityManager().getSubjects());

        date.setConverter(UtilityManager.getUtilityManager().getDateConverter());
    }

    public String getSubject() {
        return subjectBox.getValue();
    }

    public LocalDate getDate() {
        return date.getValue();
    }

    public String getStart() {
        return start.getText();
    }

    public String getEnd() {
        return end.getText();
    }

    public void setSubject(String subject) {
        if (subject != null && subjectBox.getItems().contains(subject)) {
            subjectBox.getSelectionModel().select(subject);
        }
    }

    public void setDate(LocalDate date) {
        this.date.setValue(date);
    }

    public void setStart(String start) {
        this.start.setText(start);
    }

    public void setEnd(String end) {
        this.end.setText(end);
    }
}
