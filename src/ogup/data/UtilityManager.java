package ogup.data;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import ogup.Main;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.function.Consumer;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Matti on 2019-02-01.
 * » http://schmidtchen.me «
 */
public class UtilityManager {

    private static UtilityManager utilityManager = null;

    private final Map<Integer, String> calculationGrade = new HashMap<>();
    private final HashMap<Integer, String> gradeNames = new HashMap<>();

    private boolean closeAlert = false;

    public static final int FONT_SIZE_CHANGE_WIDTH = 700;

    public static UtilityManager getUtilityManager() {
        if (utilityManager == null) {
            utilityManager = new UtilityManager();
        }

        return utilityManager;
    }

    private UtilityManager() {
        calculationGrade.put(0, "6");
        calculationGrade.put(1, "5-");
        calculationGrade.put(2, "5");
        calculationGrade.put(3, "5+");
        calculationGrade.put(4, "4-");
        calculationGrade.put(5, "4");
        calculationGrade.put(6, "4+");
        calculationGrade.put(7, "3-");
        calculationGrade.put(8, "3");
        calculationGrade.put(9, "3+");
        calculationGrade.put(10, "2-");
        calculationGrade.put(11, "2");
        calculationGrade.put(12, "2+");
        calculationGrade.put(13, "1-");
        calculationGrade.put(14, "1");
        calculationGrade.put(15, "1+");

        gradeNames.put(1, "Sehr gut");
        gradeNames.put(2, "Gut");
        gradeNames.put(3, "Befriedigend");
        gradeNames.put(4, "Ausreichend");
        gradeNames.put(5, "Mangelhaft");
        gradeNames.put(6, "Ungenügend");
    }

    public void createTables() {
        String sqlConsiderations = "CREATE TABLE IF NOT EXISTS considerations (grade INT, consideration TEXT);";

        String sqlGraduations = "CREATE TABLE IF NOT EXISTS graduations (" +
                "protocol_id INTEGER PRIMARY KEY," +
                "student_name VARCHAR," +
                "birthday DATE," +
                "birthplace VARCHAR," +
                "subject_id INT," +
                "graduation_date DATE," +
                "time_start TIME," +
                "time_end TIME," +
                "chairman VARCHAR," +
                "examining_teacher VARCHAR," +
                "secretary VARCHAR," +
                "further_members TEXT," +
                "entitled_to_vote BOOLEAN," +
                "qualified_observers TEXT," +
                "further_observers TEXT," +
                "considerations TEXT," +
                "comments TEXT," +
                "points INT," +
                "FOREIGN KEY (subject_id) REFERENCES subjects (subject_id));";

        String sqlSubjects = "CREATE TABLE IF NOT EXISTS subjects (" +
                "subject_id INTEGER PRIMARY KEY," +
                "subject VARCHAR);";

        executeStatement(sqlConsiderations);
        executeStatement(sqlGraduations);
        executeStatement(sqlSubjects);

        System.out.println("Datenbank eingerichtet!");
    }

    public Connection getDatabaseConnection() {
        Preferences preferences = Preferences.userNodeForPackage(Main.class);
        File file = new File(preferences.get("directory", System.getProperty("user.dir")) + "/OGUP");
        file.mkdirs();
        try {
            return DriverManager.getConnection("jdbc:sqlite:" +  file.getAbsolutePath() + "/OralGraduations.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet getResult(String query) {
        Connection connection = getDatabaseConnection();
        try {
            Statement statement = connection.createStatement();

            return statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public void executeStatement(String query) {
        try {
            Connection connection = getDatabaseConnection();

            Statement statement = connection.createStatement();
            statement.execute(query);

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement prepareStatement(String query) {
        try {
            Connection connection = getDatabaseConnection();

            return connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String calculateGrade(int points) {
        return calculationGrade.getOrDefault(points, "");
    }

    public HashMap<Integer, String> getGradeNames() {
        return gradeNames;
    }

    public List<String> getConsiderations (int grade) {
        ResultSet resultSet = getResult("SELECT consideration FROM considerations WHERE grade = " + grade);

        List<String> considerations = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    considerations.add(resultSet.getString("consideration"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return considerations;
    }

    public List<String> getSubjects() {
        ResultSet resultSet = getResult("SELECT * FROM subjects");

        List<String> subjects = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    subjects.add(resultSet.getString("subject"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return subjects;
    }

    public boolean isCloseAlert() {
        return closeAlert;
    }

    public void setCloseAlert(boolean closeAlert) {
        this.closeAlert = closeAlert;
    }

    public void confirmDiscard(Consumer<Boolean> result) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Schließen bestätigen");
        alert.setHeaderText("Fenster wirklich schließen?");
        alert.setContentText("Ungespeicherte Änderungen können nicht widerrufen werden!");

        Optional<ButtonType> buttonType = alert.showAndWait();
        buttonType.ifPresent(type -> result.accept(type == ButtonType.OK));
    }

    public void discardChanges(ActionEvent event) {
        confirmDiscard(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean result) {
                if (result) {
                    try {
                        Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        BorderPane home = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));

                        currentStage.setTitle("OGUP");
                        currentStage.setScene(new Scene(home, currentStage.getWidth(), currentStage.getHeight()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public HBox createInputItem(String text) {
        HBox inputItem = new HBox(10.0);
        inputItem.setAlignment(Pos.CENTER);

        TextField inputField = new TextField();
        HBox.setHgrow(inputField, Priority.ALWAYS);
        if (text != null) {
            inputField.setText(text);
        }

        Button removeButton = new Button();
        removeButton.setOnAction(this::removeItem);
        removeButton.setText("Entfernen");

        inputItem.getChildren().addAll(inputField, removeButton);

        return inputItem;
    }

    private void removeItem(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        HBox hbox = (HBox) button.getParent();
        VBox vbox = (VBox) hbox.getParent();
        vbox.getChildren().remove(hbox);
    }

    public StringConverter<LocalDate> getDateConverter() {
        return new StringConverter<LocalDate>() {
            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);

            @Override
            public String toString(LocalDate object) {
                if (object != null) {
                    return object.format(dateTimeFormatter);
                } else {
                    return null;
                }
            }

            @Override
            public LocalDate fromString(String string) {
                try {
                    return LocalDate.parse(string, dateTimeFormatter);
                } catch (DateTimeParseException e) {
                    displayFormatError();
                    return null;
                }
            }
        };
    }

    public boolean validateTime(String time) {
        Pattern pattern = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
        Matcher matcher = pattern.matcher(time);

        return matcher.matches();
    }

    public void displayFormatError() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Formatfehler");
        alert.setHeaderText("Falsches Format!");
        alert.setContentText("Das Datum oder die Zeit wurde im falschen Format angegeben (Richtiges Format: dd.MM.yyyy hh:mm)!\n\n" +
                "Hinweis: Beim Datum können Sie auch den interaktiven Kalender rechts neben dem Feld verwenden.");

        alert.showAndWait();
    }

}
